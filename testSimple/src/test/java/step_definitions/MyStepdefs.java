package step_definitions;

import applications.CertificateDataVerificationPage;
import applications.CreateCertificates;
import pageObjectModule.LoginPage;
import cucumber.api.java8.En;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;


public class MyStepdefs implements En {
    private String URL;
    private String userName;
    private String password;

    public MyStepdefs() throws IOException {
        FileReader reader=new FileReader("src/main/java/testData/property.properties");
        Properties read=new Properties();
        read.load(reader);
        URL=read.getProperty("url");
        userName=read.getProperty("userName");
        password=read.getProperty("password");
        LoginPage loginPageObj=new LoginPage();
        CreateCertificates createCertificate=new CreateCertificates();
        CertificateDataVerificationPage dataVerificationPage=new CertificateDataVerificationPage();



        Given("^login into application$", () -> {
            loginPageObj.launchApplication(URL,userName,password);
        });
        When("^enter product details and customer details$", () -> {

            createCertificate.SelectProductLists();
            createCertificate.fillTheForm();

        });
        Then("^Verify the customer and product details$", () -> {
            dataVerificationPage.verifyTheCustomerDateBeforePayment();

        });
        And("^make payment$", () -> {
            dataVerificationPage.paymentSelection();
        });
    }
}
