package applications;

import pageObjectModule.BaseClass;
import pageObjectModule.CreateCertificate;

public class CreateCertificates extends BaseClass {


    CreateCertificate cc=new CreateCertificate();

    public void SelectProductLists(){

        cc.clickCreateCertificate();
        cc.selectCategories();
        cc.selectPriceList();

    }
    public void fillTheForm(){
        cc.enterEmailID("test@gmail.com");
        cc.selectSaluation("Frau");
        cc.enterFirstName("Krishnan");
        cc.enterLastName("Audvik");
        cc.enterAddress("BoleschStrasse","87","54545","berlin");
        cc.confirmationSigns();
        cc.clickSubmitionBtn();

    }
}
