package applications;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageObjectModule.BaseClass;

public class CertificateDataVerificationPage extends BaseClass {

    @FindBy(css = "certificate-approve-category")
    static WebElement category;
    @FindBy(css = "certificate.productGroupName")
    static WebElement priceRange;
    @FindBy(css ="((//*[text()=\"Serial number:\"])[2]//following::div)[1]")
    static WebElement serialNo;
    @FindBy(css = "payment-visa")
    static WebElement VisaCard;
    @FindBy(css = "//*[text()=\"Confirm order\"]")
    static WebElement clickOrderBtn;

public void verifyTheCustomerDateBeforePayment(){

    Assert.assertEquals("Smartphone - AllianzGL",category.getText());
    Assert.assertEquals("0-250€",priceRange.getText());
    Assert.assertEquals("ASas",serialNo.getText());
}
public void paymentSelection(){
    VisaCard.click();
    clickOrderBtn.click();
}
}
