package pageObjectModule;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class CreateCertificate extends BaseClass {

@FindBy(xpath = "(//span[@class=\"text ng-binding\"])[1]")
static WebElement clickCreateCertificateBtn;
@FindBy(css = "#s2id_certificate-categories")
static WebElement selectCategory;
@FindBy(css = "#s2id_certificate-original-price")
static WebElement selectPrice;
@FindBy(css = "insurance-period list-inline list-unstyled")
static WebElement selectYears;
@FindBy(css = "certificate-product-serial")
static WebElement serialNo;
@FindBy(css = "certificate-customer-email")
static WebElement cusEmailId;
@FindBy(css ="s2id_certificate-customer-salutation")
static WebElement saluation;
@FindBy(css = "certificate-customer-firstname")
static WebElement firstName;
@FindBy(css ="certificate-customer-lastname")
static WebElement lastName;
@FindBy(css = "certificate-customer-streetname")
static WebElement streetAddress;
@FindBy(css ="certificate-customer-streetnumber")
static WebElement houseNumber;
@FindBy(css ="certificate-customer-zip")
static WebElement zipCode;
@FindBy(css="certificate-customer-city")
static WebElement cityName;
@FindBy(css = "certificate.confirmation")
static WebElement certificateConfirmationCheckBox;
@FindBy(css ="certificate.product.avb && certificate.product.pib")
static WebElement productcertificationConfirmation;
@FindBy(css = "certificate.avb_confirm")
static WebElement customerInformedAbtTearmsAndConditions;
@FindBy(css = "certificate.product_age")
static WebElement productInsuredTime;
@FindBy(css = "btn btn-success certificate-submit-button")
static WebElement clickCreateButton;



public void clickCreateCertificate(){
    PageFactory.initElements( driver, CreateCertificate.class );
    clickCreateCertificateBtn.click();
}
public void selectCategories(){
    selectCategory.click();
    Select test=new Select(selectCategory);
    test.selectByValue("Smartphone - AllianzGL");
}
public void selectPriceList(){
    Select price=new Select(selectPrice);
    price.selectByValue("0-250€");
}
public void selectYear(){
    List<WebElement> yearsList=selectYears.findElements(By.tagName("li"));
    for (WebElement li : yearsList) {
        if (li.getText().equals("1 Jahr")) {
            li.click();
        }
}

    }
public void enterSerialNo(String Snumber){
    serialNo.sendKeys(Snumber);
}
public void enterFirstName(String fName){
    firstName.sendKeys(fName);
}
public void enterLastName(String lName){
    lastName.sendKeys(lName);
}
public void selectSaluation(String saluations){
    Select sal=new Select(saluation);
    sal.selectByVisibleText(saluations);
}
public void enterEmailID(String emailID){
    cusEmailId.sendKeys(emailID);
}
public void enterAddress(String streetAdd,String houseNo,String postalCode,String city){
    streetAddress.sendKeys(streetAdd);
    houseNumber.sendKeys(houseNo);
    zipCode.sendKeys(postalCode);
    cityName.sendKeys(city);
}
public void confirmationSigns(){
    certificateConfirmationCheckBox.click();
    productcertificationConfirmation.click();
    customerInformedAbtTearmsAndConditions.click();
    productInsuredTime.click();
}
public void clickSubmitionBtn(){
    clickCreateButton.click();
}

}
